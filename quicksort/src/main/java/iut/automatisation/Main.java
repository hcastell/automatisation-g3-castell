package iut.automatisation;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Logger logger = Logger.getLogger("main");

        // Creating an array of 500 elements
        int[] randomArray = new int[500];

        // Creating a Random object to generate random integers
        Random random = new Random();

        // Populating the array with random integers
        for (int i = 0; i < randomArray.length; i++) {
            randomArray[i] = random.nextInt(); // Generates a random integer
        }


        logger.log(Level.FINE, () -> "La liste est triée ? " + (Sort.checkSort(randomArray) ? "oui" : "non")); // since Java 8, we can use Supplier , which will be evaluated lazily

        Sort.quickSort(randomArray, 0, randomArray.length - 1);

        logger.log(Level.FINE, () -> "La liste est triée ? " + (Sort.checkSort(randomArray) ? "oui" : "non")); // since Java 8, we can use Supplier , which will be evaluated lazily
    }
}